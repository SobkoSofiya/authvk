//
//  AuthWebKitMADApp.swift
//  AuthWebKitMAD
//
//  Created by Тест on 01.11.2021.
//

import SwiftUI

@main
struct AuthWebKitMADApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
