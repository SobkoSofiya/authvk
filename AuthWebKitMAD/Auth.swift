//
//  Auth.swift
//  AuthWebKitMAD
//
//  Created by Тест on 01.11.2021.
//

import Foundation
import SwiftUI
import UIKit
import WebKit

struct WebView: UIViewRepresentable {
    var request:URLRequest
    let navigate:Navigate
    func updateUIView(_ uiView: WKWebView, context: Context) {
        uiView.load(request)
    }
    
    
    func makeUIView(context: Context) -> WKWebView {
        let webView = WKWebView()
        webView.navigationDelegate = navigate
        return webView
    }
 

}
class Navigate: NSObject, ObservableObject, WKNavigationDelegate{
    @Published var token = ""
    @Published var close: (() -> ())?
    @Published var url = ""
    override init() {
      self.url = "https://oauth.vk.com/authorize?client_id=7988922&display=mobile&redirect_uri=https://oauth.vk.com/blank.html&scope=offline&response_type=token&v=5.131&state=123456&revoke=1"
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: ((WKNavigationActionPolicy) -> Void)) {
      if let url = navigationAction.request.url?.absoluteString {
        if url.contains("access_token") {
          let parts: [String] = url.split(separator: "%").map { String($0) }
          if let index: Int = parts.firstIndex(where: { $0.contains("access_token") }),
             parts.count > index + 1 {
            self.token = parts[index + 1]
              close?()
          }
        }
        decisionHandler(.allow)
      }
    }
}
