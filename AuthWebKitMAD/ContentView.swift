//
//  ContentView.swift
//  AuthWebKitMAD
//
//  Created by Тест on 01.11.2021.
//

import SwiftUI
import WebKit
struct ContentView: View {
    @State var showAuth = false
    @State var token = ""
    @StateObject var model = Navigate()
    var body: some View {
        ZStack{
            if model.token == ""{
            Button {
                showAuth.toggle()
            } label: {
                ZStack{
                    RoundedRectangle(cornerRadius: 16).frame(width: 300, height: 40, alignment: .center).foregroundColor(Color("vk"))
                    Text("Log In").foregroundColor(.white).font(.system(size: 20, weight: .medium, design: .default))
                }
            }
        
            }else{
                Text(model.token)
            }
            
        }.fullScreenCover(isPresented: $showAuth) {
            WebView( request: URLRequest(url: URL(string: "\(model.url)")!), navigate: model)
        }.onAppear {
            model.close = {
              showAuth = false
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
